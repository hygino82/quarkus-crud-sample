package br.dev.hygino.controller;

import java.util.List;
import java.util.UUID;

import br.dev.hygino.dto.GameDTO;
import br.dev.hygino.dto.GameInsertDTO;
import br.dev.hygino.service.GameService;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.DefaultValue;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/game")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GET
    public Response findAll(@QueryParam("page") @DefaultValue("0") Integer page,
            @QueryParam("pageSize") @DefaultValue("10") Integer pageSize) {
        List<GameDTO> consoles = gameService.findAll(page, pageSize);
        return Response.ok(consoles).build();
    }

    @POST
    @Transactional
    public Response insert(GameInsertDTO dto) {
        return Response.ok(gameService.createGame(dto)).build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") UUID id) {
        return Response.ok(gameService.findById(id)).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") UUID id, GameInsertDTO dto) {
        return Response.ok(gameService.update(id, dto)).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response remove(@PathParam("id") UUID id) {
        gameService.remove(id);
        return Response.ok().build();
    }
}
