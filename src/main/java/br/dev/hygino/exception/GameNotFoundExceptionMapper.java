package br.dev.hygino.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;

public class GameNotFoundExceptionMapper implements ExceptionMapper<GameNotFoundException> {
    @Override
    public Response toResponse(GameNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND.getStatusCode(), "Game not found").build();
    }
}
