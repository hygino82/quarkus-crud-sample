package br.dev.hygino.exception;

import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Provider
public class ConsoleNotFoundExceptionMapper implements ExceptionMapper<ConsoleNotFoundException> {
    @Override
    public Response toResponse(ConsoleNotFoundException exception) {
        return Response.status(Response.Status.NOT_FOUND.getStatusCode(), "Console not found").build();
    }
}
