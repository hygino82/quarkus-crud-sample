package br.dev.hygino.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_console")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class ConsoleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @NotEmpty
    private String name;

    @NotNull
    private Integer releaseYear;

    private String imgUrl;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @OneToMany(mappedBy = "console")
    private List<GameEntity> games = new ArrayList<>();

    public ConsoleEntity(@NotEmpty String name, @NotNull Integer releaseYear, String imgUrl) {
        this.name = name;
        this.releaseYear = releaseYear;
        this.imgUrl = imgUrl;
    }
}
