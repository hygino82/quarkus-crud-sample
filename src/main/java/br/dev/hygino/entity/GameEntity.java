package br.dev.hygino.entity;

import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "tb_game")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class GameEntity {
   
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private UUID id;

    @NotEmpty
    private String name;

    @NotNull
    private Integer releaseYear;

    private String imageUrl;
    private String personalCode;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;

    @ManyToOne
    @JoinColumn(name = "console_id")
    @NotNull
    private ConsoleEntity console;

    public GameEntity(@NotEmpty String name, @NotNull Integer releaseYear, String imageUrl, String personalCode) {
        this.name = name;
        this.releaseYear = releaseYear;
        this.imageUrl = imageUrl;
        this.personalCode = personalCode;
        createDate = new Date();
    }
}
