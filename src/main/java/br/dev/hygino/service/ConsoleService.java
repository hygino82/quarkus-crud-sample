package br.dev.hygino.service;

import br.dev.hygino.dto.ConsoleDTO;
import br.dev.hygino.dto.ConsoleInsertDTO;
import br.dev.hygino.entity.ConsoleEntity;
import br.dev.hygino.exception.ConsoleNotFoundException;
import br.dev.hygino.repository.ConsoleRepository;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class ConsoleService {

    private final ConsoleRepository consoleRepository;

    public ConsoleService(ConsoleRepository consoleRepository) {
        this.consoleRepository = consoleRepository;
    }

    public ConsoleDTO createConsole(ConsoleInsertDTO dto) {
        ConsoleEntity consoleEntity = dto.toEntity();
        consoleEntity.setCreateDate(new Date());
        consoleRepository.persist(consoleEntity);
        return new ConsoleDTO(consoleEntity);
    }

    public List<ConsoleDTO> findAll(Integer page, Integer pageSize) {
        return consoleRepository.findAll().page(page, pageSize).list().stream().map(x -> new ConsoleDTO(x))
                .collect(Collectors.toList());
    }

    public ConsoleDTO findById(UUID id) {
        var consoleEntity = (ConsoleEntity) consoleRepository.findByIdOptional(id)
                .orElseThrow(ConsoleNotFoundException::new);
        return new ConsoleDTO(consoleEntity);
    }

    public ConsoleDTO update(UUID id, ConsoleInsertDTO dto) {
        ConsoleEntity consoleEntity = (ConsoleEntity) consoleRepository.findByIdOptional(id)
                .orElseThrow(ConsoleNotFoundException::new);
        consoleEntity.setName(dto.name());
        consoleEntity.setImgUrl(dto.imgUrl());
        consoleEntity.setImgUrl(dto.imgUrl());
        consoleEntity.setUpdateDate(new Date());
        consoleRepository.persist(consoleEntity);
        return new ConsoleDTO(consoleEntity);
    }

    public void remove(UUID id) {
        ConsoleEntity consoleEntity = (ConsoleEntity) consoleRepository.findByIdOptional(id)
                .orElseThrow(ConsoleNotFoundException::new);
        consoleRepository.deleteById(consoleEntity.getId());
    }
}
