package br.dev.hygino.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import br.dev.hygino.dto.GameDTO;
import br.dev.hygino.dto.GameInsertDTO;
import br.dev.hygino.entity.ConsoleEntity;
import br.dev.hygino.entity.GameEntity;
import br.dev.hygino.exception.ConsoleNotFoundException;
import br.dev.hygino.exception.GameNotFoundException;
import br.dev.hygino.repository.ConsoleRepository;
import br.dev.hygino.repository.GameRepository;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GameService {

    private final GameRepository gameRepository;
    private final ConsoleRepository consoleRepository;

    public GameService(GameRepository gameRepository, ConsoleRepository consoleRepository) {
        this.gameRepository = gameRepository;
        this.consoleRepository = consoleRepository;
    }

    public GameDTO createGame(GameInsertDTO dto) {
        GameEntity gameEntity = dto.toEntity();

        ConsoleEntity consoleEntity = (ConsoleEntity) consoleRepository.findByIdOptional(dto.consoleId())
                .orElseThrow(ConsoleNotFoundException::new);

        gameEntity.setConsole(consoleEntity);
        gameRepository.persist(gameEntity);
        return new GameDTO(gameEntity);
    }

    public List<GameDTO> findAll(Integer page, Integer pageSize) {
        return gameRepository.findAll().page(page, pageSize).list().stream().map(x -> new GameDTO(x))
                .collect(Collectors.toList());
    }

    public GameDTO findById(UUID id) {
        var gameEntity = (GameEntity) gameRepository.findByIdOptional(id)
                .orElseThrow(GameNotFoundException::new);
        return new GameDTO(gameEntity);
    }

    public GameDTO update(UUID id, GameInsertDTO dto) {
        var gameEntity = (GameEntity) gameRepository.findByIdOptional(id)
                .orElseThrow(GameNotFoundException::new);
        ConsoleEntity consoleEntity = (ConsoleEntity) consoleRepository.findByIdOptional(dto.consoleId())
                .orElseThrow(ConsoleNotFoundException::new);

        copyToEntity(dto, gameEntity, consoleEntity);
        gameEntity.setUpdateDate(new Date());

        return new GameDTO(gameEntity);
    }

    private void copyToEntity(GameInsertDTO dto, GameEntity gameEntity, ConsoleEntity consoleEntity) {
        gameEntity.setName(dto.name());
        gameEntity.setImageUrl(dto.imageUrl());
        gameEntity.setReleaseYear(dto.releaseYear());
        gameEntity.setUpdateDate(new Date());
        gameEntity.setConsole(consoleEntity);
    }

    public void remove(UUID id) {
        var gameEntity = (GameEntity) gameRepository.findByIdOptional(id)
                .orElseThrow(GameNotFoundException::new);
        gameRepository.deleteById(gameEntity.getId());
    }
}
