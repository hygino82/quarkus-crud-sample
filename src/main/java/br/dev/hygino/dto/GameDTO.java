package br.dev.hygino.dto;

import java.util.Date;
import java.util.UUID;

import br.dev.hygino.entity.GameEntity;

public record GameDTO(
        UUID id,
        String name,
        Integer releaseYear,
        String imageUrl,
        String personalCode,
        String consoleName,
        Date createDate,
        Date updateDate

) {
    public GameDTO(GameEntity entity) {
        this(
                entity.getId(),
                entity.getName(),
                entity.getReleaseYear(),
                entity.getImageUrl(),
                entity.getPersonalCode(),
                entity.getConsole().getName(),
                entity.getCreateDate(),
                entity.getUpdateDate());
    }
}
