package br.dev.hygino.dto;

import java.util.Date;
import java.util.UUID;

import br.dev.hygino.entity.ConsoleEntity;

public record ConsoleDTO(
        UUID id,
        String name,
        Integer releaseYear,
        String imgUrl,
        Date createDate,
        Date updateDate

) {
    public ConsoleDTO(ConsoleEntity entity){
        this(entity.getId(),entity.getName(),entity.getReleaseYear(),entity.getImgUrl(),entity.getCreateDate(),entity.getUpdateDate());
    }
}
