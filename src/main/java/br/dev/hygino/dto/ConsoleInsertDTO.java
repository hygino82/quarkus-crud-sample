package br.dev.hygino.dto;

import br.dev.hygino.entity.ConsoleEntity;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record ConsoleInsertDTO(
                @NotEmpty String name,
                @NotNull Integer releaseYear,
                String imgUrl) {

        public ConsoleEntity toEntity() {
                return new ConsoleEntity(name, releaseYear, imgUrl);
        }
}
