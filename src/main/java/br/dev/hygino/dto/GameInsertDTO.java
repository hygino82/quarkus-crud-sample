package br.dev.hygino.dto;

import java.util.UUID;

import br.dev.hygino.entity.GameEntity;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;

public record GameInsertDTO(
        @NotEmpty String name,
        @NotNull Integer releaseYear,
        String imageUrl,
        String personalCode,
        @NotNull UUID consoleId

) {
    public GameEntity toEntity() {
        return new GameEntity(name, releaseYear, imageUrl, personalCode);
    }
}
