package br.dev.hygino.repository;

import br.dev.hygino.entity.ConsoleEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.UUID;

@ApplicationScoped
public class ConsoleRepository implements PanacheRepositoryBase<ConsoleEntity, UUID> {
}
