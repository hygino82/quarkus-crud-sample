package br.dev.hygino.repository;

import java.util.UUID;

import br.dev.hygino.entity.GameEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class GameRepository implements PanacheRepositoryBase<GameEntity, UUID> {
}
